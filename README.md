# Avro random generator
## Tool for generation of number of avro files with number of records per file with compression codec in parallel

## Usage

### Build

```

cd /path/to/avro-random-generator
mvn clean install


```

as a result - uber jar will be created: ./target/avro-random-generator-0.0.1-SNAPSHOT-1.8.1.jar

### Run

```

java -jar ./target/avro-random-generator-0.0.1-SNAPSHOT-1.8.1.jar  -s ./src/test/resources/avro-1.8.1/sample.avsc -c bzip2

```

### Options list

```

$ java -jar ./target/avro-random-generator-0.0.1-SNAPSHOT-1.8.1.jar --help
avro random generator 0.0.1
Usage: java -jar avro-random-generator-x.x.x-y.y.y.jar [options]

  -t, --threads <value>    number of generating threads, default to 3
  -s, --schema <schema.avsc>
                           schema file
  -o, --out <dir>          directory to generate files, default to ./avro-gen-2016-11-02-13-49-26, will be created if not exist
  -c, --codec null|deflate|snappy|bzip2|xz
                           codec for avro compression, default to null
  -f, --files <value>      number of generated files, default to 10
  -r, --records <value>    number of generated records per files, default to 1000
  -b, --buffer <value>     output buffer size in bytes, default to 2097152
  --help                   prints this usage text


```