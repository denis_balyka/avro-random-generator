package denik.tools.avro.generator.random

import org.apache.avro.Schema

/**
  * Created by d_balyka on 03.11.2016.
  */
trait RandomValueCalculator[V] {
  def calculate(schema: Schema, path: Seq[String]): V
}
