package denik.tools.avro.generator.random

import java.io._
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy
import java.util.concurrent.{TimeUnit, LinkedBlockingQueue, ThreadPoolExecutor}

import org.apache.avro.Schema
import org.apache.avro.Schema.Parser
import org.apache.avro.file.DataFileWriter
import org.apache.avro.generic.{GenericRecord, GenericDatumWriter}
import org.apache.avro.file.CodecFactory
import org.apache.avro.io.EncoderFactory

/**
  * Created by d_balyka on 01.11.2016.
  */
object Runner extends App {

  val sampleCfg = RunConfig()

  val parser = new scopt.OptionParser[RunConfig]("java -jar avro-random-generator-x.x.x-y.y.y.jar") {
    head("avro random generator", "0.0.1")

    opt[Int]('t', "threads").action((x, c) =>
      c.copy(threads = Some(x))).text(s"number of generating threads, default to ${sampleCfg.threads.get}")

    opt[String]('s', "schema").required().valueName("<schema.avsc>").
      action((x, c) => c.copy(schemaFile = Some(x))).
      text(s"schema file")

    opt[String]('o', "out").valueName("<dir>").
      action((x, c) => c.copy(outputDir = Some(x))).
      text(s"directory to generate files, default to ${sampleCfg.outputDir.get}, will be created if not exist")

    opt[String]('c', "codec").valueName("null|deflate|snappy|bzip2|xz").
      action((x, c) => c.copy(codec = Some(x))).
      text(s"codec for avro compression, default to ${sampleCfg.codec.get}")


    opt[Int]('f', "files").action((x, c) =>
      c.copy(numberOfFiles = Some(x))).text(s"number of generated files, default to ${sampleCfg.numberOfFiles.get}")

    opt[Int]('r', "records").action((x, c) =>
      c.copy(recordsPerFile = Some(x))).text(s"number of generated records per files, default to ${sampleCfg.recordsPerFile.get}")

    opt[Int]('b', "buffer").action((x, c) =>
      c.copy(outBufferBytes = Some(x))).text(s"output buffer size in bytes, default to ${sampleCfg.outBufferBytes.get}")

    opt[Boolean]('n', "skipHeader").action((x, c) =>
      c.copy(skipHeader = Some(x))).text(s"skip schema header, default to ${sampleCfg.skipHeader.get}")


    help("help").text("prints this usage text")


  }

  parser.parse(args, RunConfig()) match {
    case Some(config) =>
      runGenerator(config)
    case None =>
    // arguments are bad, error message will have been displayed
  }


  def runGenerator(cfg: RunConfig): Unit = {
    val parentDir = s"${cfg.outputDir.get}"
    new File(parentDir).mkdirs()


    val schema = new Parser().parse(new FileInputStream(new File(cfg.schemaFile.get)))


    val taskQueue = new LinkedBlockingQueue[Runnable](10)

    val codecFactory = CodecFactory.fromString(cfg.codec.get)

    if (cfg.threads.get > 1) {
      val exe = new ThreadPoolExecutor(cfg.threads.get - 1, cfg.threads.get - 1, 1, TimeUnit.SECONDS, taskQueue, new CallerRunsPolicy())

      for (fileIdx <- 1 to cfg.numberOfFiles.get) {

        println(s"submitting $fileIdx")

        exe.submit(new Runnable {
          override def run(): Unit = {
            generateContent(fileIdx, cfg, schema, codecFactory, parentDir)
          }
        })

      }

      //    exe.awaitTermination(10, TimeUnit.MINUTES)

      while (true) {
        if (exe.getCompletedTaskCount == cfg.numberOfFiles.get) {
          println(s"all ${cfg.numberOfFiles.get} files generated")
          System.exit(0)
        } else {
          Thread.sleep(2000)
        }
      }
    } else {
      for (fileIdx <- 1 to cfg.numberOfFiles.get) {
        println(s"submitting $fileIdx")
        generateContent(fileIdx, cfg, schema, codecFactory, parentDir)
      }
    }


  }

  def generateContent(fileIdx: Int, cfg: RunConfig, schema: Schema, codecFactory: CodecFactory, parentDir: String): Unit = {
    try {
      if (cfg.skipHeader.get) {
        saveAsAvro(fileIdx, cfg, schema, codecFactory, parentDir)
      } else {
        saveAsAvroFile(fileIdx, cfg, schema, codecFactory, parentDir)
      }

    } catch {
      case t: Throwable => t.printStackTrace()
      case _ =>
    }
  }


  def saveAsAvroFile(fileIdx: Int, cfg: RunConfig, schema: Schema, codecFactory: CodecFactory, parentDir: String): Unit = {
    println(s"started $fileIdx")
    val start = System.currentTimeMillis()
    val writer = new GenericDatumWriter[GenericRecord](null)

    val bos = new BufferedOutputStream(new FileOutputStream(s"$parentDir/part-${fileIdx}.avro"), cfg.outBufferBytes.get)

    val dataFileWriter = new DataFileWriter[GenericRecord](writer)
    dataFileWriter.setCodec(codecFactory)
    dataFileWriter.create(schema, bos)
    dataFileWriter.setSyncInterval(cfg.outBufferBytes.get)

    val avroIterator = AvroRandomGenerator.randomDataIterator(schema)

    for (idx <- 1 to cfg.recordsPerFile.get) {
      dataFileWriter.append(avroIterator.next())
    }
    dataFileWriter.flush()
    bos.flush()
    bos.close()
    println(s"$fileIdx finished in ${System.currentTimeMillis() - start}ms")
  }

  def saveAsAvro(fileIdx: Int, cfg: RunConfig, schema: Schema, codecFactory: CodecFactory, parentDir: String): Unit = {
    println(s"started $fileIdx")
    val start = System.currentTimeMillis()
    val writer = new GenericDatumWriter[GenericRecord](schema)

    val bos = new BufferedOutputStream(new FileOutputStream(s"$parentDir/part-${fileIdx}.avro"), cfg.outBufferBytes.get)

    val encoder = new EncoderFactory().directBinaryEncoder(bos, null)

    val avroIterator = AvroRandomGenerator.randomDataIterator(schema)

    for (idx <- 1 to cfg.recordsPerFile.get) {
      writer.write(avroIterator.next(), encoder)
    }
    encoder.flush()
    bos.flush()
    bos.close()
    println(s"$fileIdx finished in ${System.currentTimeMillis() - start}ms")
  }

}


case class RunConfig(
                      threads: Option[Int] = Some(Runtime.getRuntime.availableProcessors() - 1),
                      schemaFile: Option[String] = None,
                      numberOfFiles: Option[Int] = Some(10),
                      recordsPerFile: Option[Int] = Some(1000),
                      outputDir: Option[String] = Some(s"./avro-gen-${new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date())}"),
                      outBufferBytes: Option[Int] = Some(2 * 1024 * 1024),
                      skipHeader: Option[Boolean] = Some(true),
                      codec: Option[String] = Some("null"))
