package denik.tools.avro.generator.random

import java.nio.ByteBuffer
import java.util

import org.apache.avro.Schema
import org.apache.avro.generic.GenericData.{EnumSymbol, Fixed}
import org.apache.avro.generic._
import org.apache.commons.lang3.RandomStringUtils

import scala.collection.JavaConversions._
import scala.collection.Iterator
import scala.util.Random


/**
  * Created by d_balyka on 25.10.2016.
  */
object AvroRandomGenerator extends App {

  def generateRecordBySchema(schema: Schema, path: Seq[String]): GenericRecord = {

    val currentPath = path.:+(schema.getFullName)
    val builder = new GenericRecordBuilder(schema)

    schema.getFields.foreach {
      field =>
        builder.set(field, generateValue(field.schema(), currentPath.:+(field.name())))
    }

    builder.build()
  }


  def generateValue(schema: Schema, path: Seq[String]): Any = {
    val currentPath = path.:+(schema.getFullName)
    schema.getType match {
      case Schema.Type.ARRAY => {
        val count = Random.nextInt(50)
        val result = new util.ArrayList[Any]()
        result.addAll((1 to count).map { idx => generateValue(schema.getElementType, currentPath) })
        result
      }
      case Schema.Type.BOOLEAN => Random.nextBoolean()
      case Schema.Type.BYTES => ByteBuffer.wrap(RandomStringUtils.randomAlphabetic(5, 20).getBytes)
      case Schema.Type.DOUBLE => Random.nextDouble()
      case Schema.Type.ENUM => new EnumSymbol(schema, chooseRandomElement(schema.getEnumSymbols.toList))
      case Schema.Type.FIXED => new Fixed(schema, RandomStringUtils.randomAscii(schema.getFixedSize).getBytes())
      case Schema.Type.FLOAT => Random.nextFloat()
      case Schema.Type.INT => Random.nextInt()
      case Schema.Type.LONG => Random.nextLong()
      case Schema.Type.MAP => {
        val count = Random.nextInt(50)
        val result = new util.HashMap[String, Any]()
        (1 to count) foreach {
          idx =>
            result.put(RandomStringUtils.randomAlphabetic(5, 20), generateValue(schema.getValueType, currentPath))
        }
        result
      }
      case Schema.Type.NULL => null
      case Schema.Type.RECORD => generateRecordBySchema(schema, path)
      case Schema.Type.STRING => RandomStringUtils.randomAlphabetic(5, 20)
      case Schema.Type.UNION => {
        val currentType = chooseRandomElement(schema.getTypes.toList)
        generateValue(currentType, currentPath)
      }
      case _ => null
    }
  }


  def chooseRandomElement[A](elements: List[A]): A = {
    elements(Random.nextInt(elements.size))
  }

  def randomDataIterator(schema: Schema): Iterator[GenericRecord] = {
    new Iterator[GenericRecord]() {
      override def hasNext: Boolean = true

      override def next(): GenericRecord = generateRecordBySchema(schema, Seq())
    }
  }



}

