import java.io.{ByteArrayOutputStream, File, FileInputStream}

import denik.tools.avro.generator.random.AvroRandomGenerator
import org.apache.avro.Schema.Parser
import org.apache.avro.generic.{GenericRecord, GenericDatumWriter}
import org.apache.avro.io.{DecoderFactory, EncoderFactory}
import org.apache.avro.specific.SpecificDatumReader
import org.junit.{Assert, Test}
import sample.avro.model.Message

/**
  * Created by d_balyka on 01.11.2016.
  */
class TestAvroGenerator {

  @Test
  def testValidness: Unit = {
    val parser = new Parser
    val schema = parser.parse(new FileInputStream(new File("src/test/resources/avro-1.8.1/sample.avsc")))

    val it = AvroRandomGenerator.randomDataIterator(schema)

    val baos = new ByteArrayOutputStream()
    val encoder = EncoderFactory.get().directBinaryEncoder(baos, null)

    val writer = new GenericDatumWriter[GenericRecord](schema)

    writer.write(it.next(), encoder)
    encoder.flush()


    val decoder = DecoderFactory.get().binaryDecoder(baos.toByteArray, null)
    val reader = new SpecificDatumReader[Message]()

    reader.setSchema(Message.SCHEMA$)
    val msg = reader.read(null, decoder)
    Assert.assertNotNull(msg)

  }

}
