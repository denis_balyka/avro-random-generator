import denik.tools.avro.generator.random.Runner

/**
  * Created by d_balyka on 02.11.2016.
  */
object TestRunner extends App {

  Runner.main("-s ./src/test/resources/avro-1.8.1/sample.avsc -c bzip2 -t 0 -r 2".split(" "))

}
